/*
  Copyright (c) 2020 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Kotlin Coroutines_

  https://commonsware.com/Coroutines
*/

package com.commonsware.android.coroutines.widget

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.commonsware.android.coroutines.widget.databinding.ActivityMainBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.launch
import kotlinx.coroutines.suspendCancellableCoroutine

private const val TAG = "WidgetCoroutines"

class MainActivity : AppCompatActivity() {

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    val binding = ActivityMainBinding.inflate(layoutInflater)

    setContentView(binding.root)

    lifecycleScope.launch(Dispatchers.Main) {
      Log.d(TAG, "before waiting for layout")

      binding.button.waitForNextLayout()

      Log.d(TAG, "after waiting for layout")
    }

    lifecycleScope.launch(Dispatchers.Main) {
      binding.field.textEntryFlow()
        .debounce(500)
        .collect {
          Log.d(TAG, "text entered: $it")
        }
    }
  }
}

// inspired by https://chris.banes.dev/2019/12/03/suspending-views/

suspend fun View.waitForNextLayout() =
  suspendCancellableCoroutine<Unit> { continuation ->
    val onChange = object : View.OnLayoutChangeListener {
      override fun onLayoutChange(
        v: View?,
        left: Int,
        top: Int,
        right: Int,
        bottom: Int,
        oldLeft: Int,
        oldTop: Int,
        oldRight: Int,
        oldBottom: Int
      ) {
        removeOnLayoutChangeListener(this)

        continuation.resume(Unit) { }
      }
    }

    continuation.invokeOnCancellation { removeOnLayoutChangeListener(onChange) }

    addOnLayoutChangeListener(onChange)
  }

fun EditText.textEntryFlow() =
  callbackFlow<Editable> {
    val watcher = object : TextWatcher {
      override fun afterTextChanged(s: Editable) {
        offer(s)
      }

      override fun beforeTextChanged(
        s: CharSequence?,
        start: Int,
        count: Int,
        after: Int
      ) {
        // unused
      }

      override fun onTextChanged(
        s: CharSequence?,
        start: Int,
        before: Int,
        count: Int
      ) {
        // unused
      }
    }

    addTextChangedListener(watcher)

    awaitClose { removeTextChangedListener(watcher) }
  }