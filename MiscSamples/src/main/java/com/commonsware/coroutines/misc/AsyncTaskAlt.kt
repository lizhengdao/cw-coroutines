/*
  Copyright (c) 2020 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Kotlin Coroutines_

  https://commonsware.com/Coroutines
*/

package com.commonsware.coroutines.misc

import android.os.AsyncTask
import kotlinx.coroutines.*
import java.util.concurrent.Executor

fun <T> asyncTask(
  background: () -> T,
  executor: Executor = AsyncTask.THREAD_POOL_EXECUTOR,
  post: (T) -> Unit
) {
  SimpleTask(background, post).executeOnExecutor(executor)
}

class SimpleTask<T>(
  private val background: () -> T,
  private val post: (T) -> Unit
) : AsyncTask<Unit, Unit, T>() {
  override fun doInBackground(vararg params: Unit): T {
    return background()
  }

  override fun onPostExecute(result: T) {
    post(result)
  }
}

fun <T> CoroutineScope.asyncTaskAlt(
  background: () -> T,
  dispatcher: CoroutineDispatcher = AsyncTask.THREAD_POOL_EXECUTOR.asCoroutineDispatcher(),
  post: (T) -> Unit
) {
  launch(Dispatchers.Main) {
    post(withContext(dispatcher) {
      background()
    })
  }
}
