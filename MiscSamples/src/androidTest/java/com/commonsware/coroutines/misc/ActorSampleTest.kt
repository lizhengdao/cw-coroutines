package com.commonsware.coroutines.misc

import androidx.test.ext.junit.runners.AndroidJUnit4
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ActorSampleTest {
  @Test
  fun screenTestAnActor() {
    val sample = ActorSample()
    val sendChannel = sample.actOut(GlobalScope)

    GlobalScope.launch(Dispatchers.Default) {
      for (i in 0..100)  {
        sendChannel.send(i)
      }
    }

    sendChannel.close()

    val results = sample.receivedItems

    println(results)
  }
}